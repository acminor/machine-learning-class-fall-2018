import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.python import debug as tf_debug
import sklearn as sk

BATCH_SZ = 100
n = 16000

authors_k_in = tf.placeholder(shape=(BATCH_SZ, 5), dtype=tf.float32)
authors = keras.layers.Embedding(n+1, 20)(authors_k_in)
authors = keras.layers.Dense(100)(authors)
authors = keras.layers.GlobalAveragePooling1D()(authors)

author_list_name_k_in = tf.placeholder(shape=(BATCH_SZ, 10, 5), dtype=tf.float32)
author_list_org_k_in = tf.placeholder(shape=(BATCH_SZ, 10, 5), dtype=tf.float32)
author_list_name = keras.layers.Embedding(n+1, 20)(author_list_name_k_in)
author_list_org = keras.layers.Embedding(n+1, 20)(author_list_org_k_in)
author_list_name = keras.layers.Dense(100)(author_list_name)
author_list_org = keras.layers.Dense(100)(author_list_name)
author_list = keras.layers.Concatenate()([author_list_name, author_list_org])
author_list = keras.layers.GlobalAveragePooling2D()(author_list)

keywords_k_in = tf.placeholder(shape=(BATCH_SZ, 10, 3), dtype=tf.float32)
keywords = keras.layers.Embedding(n+1, 20)(keywords_k_in)
keywords = keras.layers.Dense(100)(keywords)
keywords = keras.layers.GlobalAveragePooling2D()(keywords)

title_k_in = tf.placeholder(shape=(BATCH_SZ, 10), dtype=tf.float32)
title = keras.layers.Embedding(n+1, 20)(title_k_in)
title = keras.layers.Dense(100)(title)
title = keras.layers.GlobalAveragePooling1D()(title)

abstract_k_in = tf.placeholder(shape=(BATCH_SZ, 15), dtype=tf.float32)
abstract = keras.layers.Embedding(n+1, 20)(abstract_k_in)
abstract = keras.layers.Dense(100)(abstract)
abstract = keras.layers.GlobalAveragePooling1D()(abstract)

# venue (str)
# at max 15
# -- for now ignore
# venue = keras.preprocessing.text.hashing_trick(venue, n, hash_function='md5')
# venue = keras.preprocessing.sequence.pad_sequences(venue, maxlen=15, padding='post', value=0.0)
# keras.layers.Embedding([])

# year (int) -- TODO fix
# year = [[1]]
# year = tf.convert_to_tensor(year)
# year = keras.layers.Dense(1)(year)

# id (str)

## Input transformation #######################################################################
# Author Term (str)
# at max 5 strs
authors_in = ['blah']
temp1 = np.asarray(authors_in)
authors_in = [keras.preprocessing.text.hashing_trick(i, n, hash_function='md5') for i in authors_in]
authors_in = keras.preprocessing.sequence.pad_sequences(authors_in, maxlen=5, dtype=float,
                                                     padding='post', value=0.0)

# abstract (str)
# at max 15
abstract_in = ['blah']
abstract_in = [keras.preprocessing.text.hashing_trick(i, n, hash_function='md5') for i in abstract_in]
abstract_in = keras.preprocessing.sequence.pad_sequences(abstract_in, maxlen=15, padding='post', value=0.0)

# title (str)
# at max 10
title_in = ['blah']
title_in = [keras.preprocessing.text.hashing_trick(i, n, hash_function='md5') for i in title_in]
title_in = keras.preprocessing.sequence.pad_sequences(title_in, maxlen=10, padding='post', value=0.0)

# keywords (array) of (str)
# at max 10
# at max 3
keywords_in = [['blah']]
keywords_in = [[keras.preprocessing.text.hashing_trick(keyword, n, hash_function='md5')
                for keyword in doc] for doc in keywords_in]
keywords_in = keras.preprocessing.sequence.pad_sequences(keywords_in, maxlen=10, padding='post',
                                                         value=0.0)
keywords_in = np.asarray(
    [keras.preprocessing.sequence.pad_sequences(doc, padding='post', maxlen=3, value=0.0)
     for doc in keywords_in])

# Authors List (array) of $name, $org pairs
# at max 10
# author at max 5
# org at max 5
author_list_name_in = [['blah', 'blah']]
author_list_org_in = [['bleh', 'bleh']]

author_list_name_in = [[keras.preprocessing.text.hashing_trick(author, n, hash_function='md5')
                        for author in doc] for doc in author_list_name_in]
author_list_org_in = [[keras.preprocessing.text.hashing_trick(org, n, hash_function='md5')
                       for org in doc] for doc in author_list_org_in]

author_list_name_in = keras.preprocessing.sequence.pad_sequences(author_list_name_in,
                                                              padding='post', maxlen=10, value=0.0)
author_list_org_in = keras.preprocessing.sequence.pad_sequences(author_list_org_in,
                                                             padding='post', maxlen=10, value=0.0)

author_list_name_in = np.asarray(
    [keras.preprocessing.sequence.pad_sequences(doc, padding='post', maxlen=5, value=0.0)
     for doc in author_list_name_in])
author_list_org_in = np.asarray(
    [keras.preprocessing.sequence.pad_sequences(doc, padding='post', maxlen=5, value=0.0)
     for doc in author_list_org_in])

## Model ###################################################################################
# cluster should group by author
# y_pred, y_real
def pairwise_loss(z_vecs, real_clusters_authors):
    z_vecs = z_vecs[:, 0:500]
    n_clusters = z_vecs[:, 500]
    n_clusters = int(sum(n_clusters))
    authors = z_vecs[:, 501]
    paper_ids = z_vecs[:, 502]

    clusters = sk.cluster.MiniBatchKMeans(n_clusters).fit(z_vecs)
    docs_to_authors = clusters.predict(z_vecs)

    same_clusters = {author: [[] for i in range(0, clusters)] for author in authors}
    for author, paper_id, cluster in zip(authors, paper_ids, docs_to_authors):
        same_clusters[author][cluster].append(paper_id)

    # maximize the f1 predictions of all clusters
    metric = 0.0
    for author in same_clusters.keys():
        for cluster in same_clusters.values():
            max_pred_cluster = 0.0
            for real_cluster in real_clusters_authors[author]:
                correct_preds = sum([1 for id in cluster if id in real_cluster])
                precision = correct_preds / float(len(cluster))
                recall = correct_preds / float(len(real_cluster))
                # mod to scale recall to density
                # TODO TOT_SIZE
                recall = (BATCH_SZ/TOT_SIZE)*recall
                f1 = (2*precision * recall) / (precision + recall)
                max_pred_cluster = f1 \
                    if f1 > max_pred_cluster else max_pred_cluster
            metric += max_pred_cluster
    # for cost (min)
    return -1*metric

authors_k_in_str = tf.placeholder(shape=(BATCH_SZ, 1))
paper_id_k_in_str = tf.placeholder(shape=(BATCH_SZ, 1))

model = keras.layers.Concatenate()([authors, author_list, keywords, title, abstract])#, year)
model = keras.layers.Dense(1000)(model)
z_vec = keras.layers.Dense(500)(model)
n_clusters = keras.layers.Dense(1)(model)
n_clusters = tf.math.reduce_sum(n_clusters)

# thanks https://stackoverflow.com/questions/44172165/keras-multiple-output-custom-loss-function
model = keras.layers.Concatenate()([z_vec, n_clusters, authors_k_in_str, paper_id_k_in_str])

# model = tf.contrib.factorization.KMeans(z_vec,
#                                         tf.to_int32(n_clusters)).training_graph()
# model = tf.train.limit_epochs(model[5], num_epochs=100)
# model = tf.placeholder(tensor=model)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializers())

    for epoch in range(0, self.EPHOCS):
        # TODO shuffle
        for j in range(0, len(authors), BATCH_SZ): # TODO length of batch
            batch_authors = None
            batch_author_list_name = None
            batch_author_list_org = None
            batch_keywords = None
            batch_title = None
            batch_abstract = None
            batch_authors_str = None
            batch_paper_id_str = None
            # TODO GET INFO
            sess.run([],
                     feed_dict={
                         'authors_k_in:0': batch_authors,
                         'author_list_name_k_in:0': batch_author_list_name,
                         'author_list_org_k_in:0': batch_author_list_org,
                         'author_k_in:0': batch_keywords,
                         'title_k_in:0': batch_title,
                         'abstract_k_in:0': batch_abstract,
                         'authors_k_in_str:0', batch_authors_str,
                         'paper_id_k_in_str:0', batch_paper_id_str
                     })

model = keras.models.Model(inputs=[authors_k_in, author_list_name_k_in, author_list_org_k_in,
                                   keywords_k_in, title_k_in, abstract_k_in,
                                   authors_k_in_str, paper_id_k_in_str],
                           outputs=[model])
model.summary()
sess = keras.backend.get_session()
sess = tf_debug.LocalCLIDebugWrapperSession(sess)
keras.backend.set_session(sess)

model.compile(optimizer=tf.train.AdamOptimizer(0.01),
              loss = 'binary_crossentropy',
              # loss=pairwise_loss,
              metrics=['accuracy'])

# filter labels to fit match function TODO and rework custom cluster check
model.fit([authors_in,
        author_list_name_in,
        author_list_org_in,
        keywords_in,
        title_in,
        abstract_in,
        temp1,
        np.asarray(['id-asdlkj'])],
# {
#     'authors_k_in': authors_in,
#     'author_list_name_k_in': author_list_name_in,
#     'author_list_org_k_in': author_list_org_in,
#     'keywords_k_in': keywords_in,
#     'title_k_in': title_in,
#     'abstract_k_in': abstract_in,
#     'authors_k_in_str': temp1,
#     'paper_id_k_in_str': ['id-asdlkj']
# },
        {'blah': ['id-asdlkj']}, batch_size=BATCH_SZ)
