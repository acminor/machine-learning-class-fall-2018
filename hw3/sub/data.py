import json
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin

def load_data(filepath):
    data = json.load(open(filepath))

    # load the data as a Bag of Words Document
    out_data = {
        'id': [],
        'author_searched': [],
        'docs': []
    }

    for key in data.keys():
        for doc in data[key]:
            author_searched = key.replace('_', ' ')
            author_searched = author_searched.split(' ')
            author_searched = [i.capitalize() for i in author_searched]
            author_searched = ' '.join(author_searched)

            if type(doc.get('authors')) == dict:
                vals = doc.get('authors')
                authors = ['{} from {}'.format(vals['name'], vals['org'])]
            else:
                authors = ['{} from {}'.format(vals['name'], vals['org'])
                           for vals in doc.get('authors')]
            authors = ', '.join(authors)

            out_doc = ('Author searched for is {}. '
                    'Authors in the material are {}. '
                    'The title of the document is {}. '
                    'The abstract is {}. '
                    'The keywords are {}. '
                    'The venue used is {}. '
                    'The year released is {}.').format(
                        author_searched,
                        authors,
                        doc.get('title', 'None'),
                        doc.get('abstract'),
                        ', '.join(doc.get('keywords', ['None'])),
                        doc.get('venue', 'None'),
                        doc.get('year', 'None')
                    )
            out_id = doc['id']
            out_data['id'].append(out_id)
            out_data['author_searched'].append(key)
            out_data['docs'].append(out_doc)
    return out_data

def load_labels(filepath):
    data = json.load(open(filepath))

    out_data = {
        'author_searched': [],
        'id_sets': []
    }

    for key in data.keys():
        out_data['author_searched'].append(key)
        out_data['id_sets'].append(data[key])
    return out_data

def todense(X):
    return X.todense()

def toabs(X):
    return np.absolute(X)

def towlists(X):
    X = [i.split(' ') for i in X]
    return X

def dump_labels(filepath, all_labels):
    out = {}
    for author, ids_batch, clusters in all_labels:
        preds = zip(ids_batch, clusters)
        temp = [i for i in preds]
        preds = []
        for t in {i[1] for i in temp}:
            preds.append([i[0] for i in temp if i[1] == t])
        out[author] = preds
    json.dump(out, open(filepath))
