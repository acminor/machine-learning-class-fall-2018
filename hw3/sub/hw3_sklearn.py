from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.cluster import Birch, MiniBatchKMeans, AffinityPropagation, DBSCAN, MeanShift, SpectralClustering, AgglomerativeClustering, KMeans
from sklearn.manifold import Isomap, LocallyLinearEmbedding
from sklearn import metrics
from sklearn.feature_extraction.text import HashingVectorizer, TfidfTransformer, CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import FunctionTransformer
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.decomposition import TruncatedSVD, LatentDirichletAllocation, KernelPCA
from sklearn.mixture import BayesianGaussianMixture, GaussianMixture
from sklearn.preprocessing import PowerTransformer, QuantileTransformer
from sklearn.gaussian_process.kernels import Matern
from sklearn.kernel_approximation import Nystroem, AdditiveChi2Sampler, RBFSampler
from sklearn.gaussian_process.kernels import RBF
from gensim.sklearn_api import D2VTransformer, W2VTransformer
from joblib import Memory
import data
import numpy as np
import scipy as sp
import json
import tensorflow as tf
from nevergrad.optimization import optimizerlib
from concurrent import futures
import click

# https://stackoverflow.com/questions/35911252/disable-tensorflow-debugging-information
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def tf_graph(build=False):
    gi = tf.placeholder(dtype=tf.float32, name='graph_input', shape=(None, 120))
    # w1 = tf.placeholder(name='WEIGHTS_1')
    # w2 = tf.placeholder(name='WEIGHTS_2')
    # w3 = tf.placeholder(name='WEIGHTS_3')
    layer1 = tf.layers.Dense(500, activation=tf.nn.relu)
    layer2 = tf.layers.Dense(500, activation=tf.nn.relu)
    layer3 = tf.layers.Dense(120, activation=tf.sigmoid)
    layercount = tf.layers.Dense(1, activation=tf.sigmoid)
    if build:
        layer1.build((None, 120))
        layer2.build((None, 500))
        layer3.build((None, 500))
        layercount.build((None, 120))
        l1_weight_sz = layer1.count_params()
        l2_weight_sz = layer2.count_params()
        l3_weight_sz = layer3.count_params()
        lc_weight_sz = layercount.count_params()
    else:
        l1_weight_sz = -1
        l2_weight_sz = -1
        l3_weight_sz = -1
        lc_weight_sz = -1
    layer1_l = layer1(gi)
    layer2_l = layer2(layer1_l)
    layer3_l = layer3(layer2_l)
    layercount_l = layercount(layer3_l)
    total_weights = l1_weight_sz+l2_weight_sz+l3_weight_sz+lc_weight_sz
    return (layer1, layer2, layer3, layercount, total_weights,
            l1_weight_sz, l2_weight_sz, l3_weight_sz, lc_weight_sz,
            layer3_l, layercount_l, gi)
# int. session advice from https://stackoverflow.com/questions/36007883/tensorflow-attempting-to-use-uninitialized-value-in-variable-initialization
# sess = tf.InteractiveSession()
total_weights, l1_weight_sz, l2_weight_sz, l3_weight_sz, lc_weight_sz = tf_graph(True)[4:9]
sess_iter = 0

def TF_NN_NEVER_GRAD(dim_red_data, weights):
    global sess_iter
    with tf.Session() as sess:
        (layer1, layer2, layer3, layercount, _,
         _, _, _, _, layer3_l, layercount_l, gi) = tf_graph()

        w1 = weights[0:l1_weight_sz]
        w2 = weights[l1_weight_sz:l1_weight_sz+l2_weight_sz]
        w3 = weights[l1_weight_sz+l2_weight_sz:
                l1_weight_sz+l2_weight_sz+l3_weight_sz]
        wc = weights[l1_weight_sz+l2_weight_sz+l3_weight_sz:
                l1_weight_sz+l2_weight_sz+l3_weight_sz+lc_weight_sz]

        # TODO reshape weights to 120x500 and
        # assign to np.array pos 0 then copy get_weights pos 1 to pos 1
        w1 = [np.reshape(w1[0:np.multiply.reduce(layer1.get_weights()[0].shape)],
                            newshape=layer1.get_weights()[0].shape),
                np.array(w1[np.multiply.reduce(layer1.get_weights()[0].shape):])]
        w2 = [np.reshape(w2[0:np.multiply.reduce(layer2.get_weights()[0].shape)],
                            newshape=layer2.get_weights()[0].shape),
                np.array(w2[np.multiply.reduce(layer2.get_weights()[0].shape):])]
        w3 = [np.reshape(w3[0:np.multiply.reduce(layer3.get_weights()[0].shape)],
                            newshape=layer3.get_weights()[0].shape),
                np.array(w3[np.multiply.reduce(layer3.get_weights()[0].shape):])]
        wc = [np.reshape(wc[0:np.multiply.reduce(layercount.get_weights()[0].shape)],
                            newshape=layercount.get_weights()[0].shape),
                np.array(wc[np.multiply.reduce(layercount.get_weights()[0].shape):])]
        # w2 = np.array([np.reshape(w1, [500,500]), layer2.get_weights()[1]])
        # w3 = np.array([np.reshape(w1, [500,120]), layer3.get_weights()[1]])
        # wc = np.array([np.reshape(w1, [500,1]), layercount.get_weights()[1]])
        layer1.set_weights(w1)
        layer2.set_weights(w2)
        layer3.set_weights(w3)
        layercount.set_weights(wc)

        sess_iter = 1
        l3_out, lc_out = sess.run([layer3_l, layercount_l], feed_dict={
            gi.name: dim_red_data
        })

        # print('here :)')

        # print(lc_out)
        # for sanity assume no more than 200 clusters
        lc_out = abs(int(np.sum(lc_out))%200) + 2 # plus 2 for min value of 2 clusters
        # lc_out = int(np.sum(lc_out)) + 1
        # print(lc_out)

        # text_clst = AffinityPropagation(damping=.9)
        text_clst = MiniBatchKMeans(n_clusters=lc_out)
        return text_clst.fit_predict(l3_out)

def f1_mod(labels, predicted):
    def f1(label, pred):
        matched = len([i for i in pred if i in label])
        precision = matched/len(pred)
        recall = matched/len(label)

        # assume f1 = 0 for 0 precision and recall
        if (precision + recall) == 0:
            return 0

        f_1 = (2*precision*recall)/(precision + recall)
        return f_1

    # case where labels and predicted are groupings
    metric = 0
    for pred in predicted:
        max_f1 = -1
        for label in labels:
            f1_val = f1(label, pred)
            max_f1 = max_f1 if max_f1 > f1_val else f1_val
        metric += max_f1

    return metric

def testing():
    train = data.load_data('/home/austin/pubs_train.json')
    labels = data.load_labels('/home/austin/assignment_train.json')
    # def todense(X):
    #     return X.todense()

    # __main__.todense = todense

    # cache = Memory('./cache')
    text_clst = Pipeline([
        ('vect', HashingVectorizer(analyzer='char_wb')),
        ('tfid', TfidfTransformer()),
        # ('pre', FunctionTransformer(data.towlists)),
        # ('vect', W2VTransformer(min_count=1,
        #                         size=101, window=10)),
        # cite TODO
        # ('blah', FunctionTransformer(data.todense,
        #                              accept_sparse=True,
        #                              validate=True)),
        # # ('embed', Isomap(n_jobs=2)),
        # ('trans', PowerTransformer()),
        # ('trans', QuantileTransformer(
        #     ignore_implicit_zeros=True)),
        # ('trans2', Nystroem(n_components=101)),
        ('embed', TruncatedSVD(n_components=100)),
        # ('trans2', RBFSampler(n_components=99)),
        # ('blah2', FunctionTransformer(data.toabs,
        #                              accept_sparse=True,
        #                              validate=True)),
        # ('emded', LatentDirichletAllocation(n_components=100)),
        # ('clst', Birch())
        # ('clst', DBSCAN())
        # ('clst', MeanShift())
        ('clst', AffinityPropagation())
        # ('clst', BayesianGaussianMixture(n_components=100))
        # ('clst', MiniBatchKMeans())
                        #n_clusters=len(labels['id_sets'])))
    ])#, memory='/media/austin/3721da1a-c936-4469-9fd9-956d44a6d668/home/austin/ml_cache')

    print('Start')

    authors = set(labels['author_searched'])
    authors = sorted(authors,
            key=lambda author: len([1 for a in train['author_searched'] if a == author]))
    #authors = [i for i in zip(authors,[len([1 for a in train['author_searched'] if a == author]) for author in authors])]
    #print(authors)
    #exit()

    for author in authors:#set(labels['author_searched']):
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        labs = None
        # for i in filter(lambda i: labels['author_searched'][i[0]] == author,
        #                     enumerate(labels['id_sets'])):
        #     labs.append(i[1])
        for i, val in enumerate(labels['id_sets']):
            if labels['author_searched'][i] == author:
                labs = val
                break

        ids_batch = [train['id'][i] for i in indexes_used]

        def f1_wrapper(predictor, docs, **kwargs):
            # predicted = predictor.predict(docs)
            print(type(predictor))
            predicted = predictor.fit_predict(docs)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])

            f1_val = f1_mod(labs, preds)
            print('F1 metric for {} yields {}'.format(author, f1_val))
            return f1_val


        params = {
        # 'clst__n_clusters': [i for i in range(80, 100)]
            'vect__ngram_range': [(2,2), (3,3), (4,4), (5,5)],
            'vect__n_features': [10000, 40000, 50000],
            'vect__norm': ['l1', 'l2', None],
    #        'tfid__sublinear_tf': [True, False],
    #        'tfid__use_idf': [True, False],
            'tfid__norm': ['l1', 'l2', None],
            'embed__n_components': [100, 120],
            'clst__damping': [.5, .9]
        }
        gs_clst = GridSearchCV(text_clst, params, cv=5,
                            iid=False, n_jobs=4,
                            scoring=f1_wrapper, verbose=100)
        predicted = gs_clst.fit(train_batch)
        print('Best F1 metric for {} yields {} with params {}'.format(author, predicted.best_score_, predicted.best_params_))

def running():
    # train = data.load_data('/home/austin/pubs_validate.json')
    train = data.load_data('/home/austin/pubs_train.json')
    labels = data.load_labels('/home/austin/assignment_train.json')

    text_clst = Pipeline([
        # ('vect', HashingVectorizer(analyzer='char_wb', non_negative=True)),
        # ('tfid', TfidfTransformer()),
        # ('vect', W2VTransformer(size=240)),
        ('vect', D2VTransformer(size=240)),
        # ('blah', FunctionTransformer(lambda X: print(X))),
        # ('embed', TruncatedSVD(n_components=100)),
        ('kernel', KernelPCA(n_components=100)),
        # ('manifold', Isomap()),
        # ('manifold', LocallyLinearEmbedding()),
        # ('clstpre', Birch(n_clusters=150)),
        ('clst', AffinityPropagation())
    ])

    print('Start')

    authors = set(train['author_searched'])
    authors = sorted(authors,
            key=lambda author: len([1 for a in train['author_searched'] if a == author]))

    out = {}
    for author in authors:
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        labs = None
        for i, val in enumerate(labels['id_sets']):
            if labels['author_searched'][i] == author:
                labs = val
                break

        ids_batch = [train['id'][i] for i in indexes_used]

        def preds_wrapper(predictor, docs, **kwargs):
            predicted = predictor.fit_predict(docs)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])
            print('Done...')
            return preds

        def f1_wrapper(predictor, docs, **kwargs):
            # predicted = predictor.predict(docs)
            print(type(predictor))
            predicted = predictor.fit_predict(docs)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])

            f1_val = f1_mod(labs, preds)
            print('F1 metric for {} yields {}'.format(author, f1_val))
            return f1_val

        params = {
            # 'vect__ngram_range': (5,5),
            # 'vect__n_features': 50000,
            # 'vect__norm': 'l1',
            # 'tfid__norm': 'l2',
            # 'embed__n_components': 140,
            # 'embed__algorithm': 'arpack',
            # 'embed__n_iter': 10
            'clst__damping': .9
            # 'manifold__n_components': 3
            # 'kernel__kernel': 'rbf'
            # 'kernel__gamma': .2
        }
        text_clst.set_params(**params)
        # predicted = text_clst.fit(train_batch)

        train_batch = [i.lower().split(' ') for i in train_batch]
        f1_wrapper(text_clst, train_batch)

        # preds = preds_wrapper(text_clst, train_batch)
        # out[author] = preds
    json.dump(out, open('./predicted_clusters.json', 'w+'))

def running2(blah):
    if blah:
        #train = data.load_data('/home/austin/pubs_validate.json')
        train = data.load_data('/home/austin/pubs_test.json')
    else:
        train = data.load_data('/home/austin/pubs_train.json')
        labels = data.load_labels('/home/austin/assignment_train.json')

    text_clst = Pipeline([
        ('vect', CountVectorizer(analyzer='char_wb', ngram_range=(4,4))),
        ('tfid', TfidfTransformer()),
        ('embed', TruncatedSVD(n_components=120)),
        ('clst', AffinityPropagation())
    ])

    print('Start')

    authors = set(train['author_searched'])
    authors = sorted(authors,
            key=lambda author: -1*len([1 for a in train['author_searched'] if a == author]))

    out = {}
    for author in authors:
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        labs = None
        if not blah:
            for i, val in enumerate(labels['id_sets']):
                if labels['author_searched'][i] == author:
                    labs = val
                    break

        ids_batch = [train['id'][i] for i in indexes_used]

        def preds_wrapper(predictor, docs, **kwargs):
            predicted = predictor.fit_predict(docs)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])
            print('Done...')
            return preds

        def f1_wrapper(predictor, docs, **kwargs):
            # predicted = predictor.predict(docs)
            print(type(predictor))
            predicted = predictor.fit_predict(docs)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])

            f1_val = f1_mod(labs, preds)
            print('F1 metric for {} yields {}'.format(author, f1_val))
            return f1_val

        params = {
            'clst__damping': .9,
        }
        text_clst.set_params(**params)
        # predicted = text_clst.fit(train_batch)

        if not blah:
            f1_wrapper(text_clst, train_batch)

        if blah:
            preds = preds_wrapper(text_clst, train_batch)
            out[author] = preds
    json.dump(out, open('./predicted_clusters.json', 'w+'))

R4_BEST_METRIC = 0
def r4_f1_wrapper(precomp_text_pre_proc, weights):
    '''
    precomp_text_pre_proc = [{ids: [ids], docs: [docs], labs: [labels]}, ...]
    '''
    global R4_BEST_METRIC

    out = {}
    f1_vals = []
    for author in precomp_text_pre_proc:
        red_train = author['docs']
        ids_batch = author['ids']
        labs = author['labs']

        predicted = TF_NN_NEVER_GRAD(red_train, weights)
        preds = zip(ids_batch, predicted)
        temp = [i for i in preds]
        preds = []
        for t in {i[1] for i in temp}:
            preds.append([i[0] for i in temp if i[1] == t])

        f1_vals.append(f1_mod(labs, preds))
    metric = np.average(f1_vals)
    print('F1 metric yields {}'.format(metric))

    if metric > R4_BEST_METRIC:
        R4_BEST_METRIC = metric
        np.array(weights).dump('best_weights_so_far')
    return metric

def r5_wrapper(precomp_text_pre_proc, weights):
    '''
    precomp_text_pre_proc = [{ids: [ids], docs: [docs], labs: [labels]}, ...]
    '''
    out = {}
    f1_vals = []
    for author in precomp_text_pre_proc:
        red_train = author['docs']
        ids_batch = author['ids']
        labs = author['labs']

        predicted = TF_NN_NEVER_GRAD(red_train, weights)
        preds = zip(ids_batch, predicted)
        temp = [i for i in preds]
        preds = []
        for t in {i[1] for i in temp}:
            preds.append([i[0] for i in temp if i[1] == t])

        try:
            f1_vals.append(metrics.silhouette_score(red_train, predicted))
        except:
            f1_vals.append(-10000)
    metric = np.average(f1_vals)
    print('Silhouette metric yields {}'.format(metric))
    return metric

def running3(blah):
    if blah:
        train = data.load_data('/home/austin/pubs_validate.json')
    else:
        train = data.load_data('/home/austin/pubs_train.json')
        labels = data.load_labels('/home/austin/assignment_train.json')

    print('Start')

    authors = set(train['author_searched'])
    authors = sorted(authors,
            key=lambda author: len([1 for a in train['author_searched'] if a == author]))

    if blah:
        weights = np.load('best_weights_so_far')

    out = {}
    for author in authors:
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        text_pre_proc = CountVectorizer(analyzer='char_wb',
                                        ngram_range=(4,4)).fit_transform(train_batch)
        text_pre_proc = TfidfTransformer().fit_transform(text_pre_proc)
        text_pre_proc = TruncatedSVD(n_components=120).fit_transform(text_pre_proc)

        labs = None
        if not blah:
            for i, val in enumerate(labels['id_sets']):
                if labels['author_searched'][i] == author:
                    labs = val
                    break

        ids_batch = [train['id'][i] for i in indexes_used]

        def preds_wrapper(red_train, **kwargs):
            predicted = TF_NN_NEVER_GRAD(red_train, weights)
            # predicted = predictor.fit_predict(docs)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])
            print('Done...')
            return preds

        def f1_wrapper(red_train, weights, **kwargs):
            # predicted = predictor.fit_predict(docs)
            # print(red_train)
            predicted = TF_NN_NEVER_GRAD(red_train, weights)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])

            f1_val = f1_mod(labs, preds)
            print('F1 metric for {} yields {}'.format(author, f1_val))
            return f1_val

        if not blah:
            red_train = text_pre_proc
            optimizer = optimizerlib.PSO(dimension=total_weights, budget=1000, num_workers=5)
            with futures.ThreadPoolExecutor(max_workers=optimizer.num_workers) as executor:
                recommendation = optimizer.optimize(
                    lambda weights: -1*f1_wrapper(red_train, weights), executor=executor)

                # print(recommendation)
                recommendation = np.array(recommendation)
                recommendation.dump('train_weights')
                # np.savez(open('train_weights.npz', 'w+'), recommendation)
                exit(0)

        if blah:
            preds = preds_wrapper(text_pre_proc)
            out[author] = preds

    json.dump(out, open('./predicted_clusters.json', 'w+'))

def running4():
    train = data.load_data('/home/austin/pubs_train.json')
    labels = data.load_labels('/home/austin/assignment_train.json')

    print('Start')

    authors = set(train['author_searched'])
    # authors = sorted(authors,
    #         key=lambda author: len([1 for a in train['author_searched'] if a == author]))
    authors = np.array(list(authors))
    np.random.shuffle(authors)

    precomp_text_pre_proc = []
    for author in authors[0:7]:
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        text_pre_proc = CountVectorizer(analyzer='char_wb',
                                        ngram_range=(4,4)).fit_transform(train_batch)
        text_pre_proc = TfidfTransformer().fit_transform(text_pre_proc)
        text_pre_proc = TruncatedSVD(n_components=120).fit_transform(text_pre_proc)

        labs = None
        for i, val in enumerate(labels['id_sets']):
            if labels['author_searched'][i] == author:
                labs = val
                break

        ids_batch = [train['id'][i] for i in indexes_used]
        precomp_text_pre_proc.append({
            'author': author,
            'docs': text_pre_proc,
            'ids': ids_batch,
            'labs': labs
        })

    optimizer = optimizerlib.NoisyBandit(dimension=total_weights, budget=500, num_workers=5)
    # optimizer = optimizerlib.OnePlusOne(dimension=total_weights, budget=500, num_workers=5)
    # optimizer = optimizerlib.PSO(dimension=total_weights, budget=500, num_workers=5)
    with futures.ThreadPoolExecutor(max_workers=optimizer.num_workers) as executor:
        recommendation = optimizer.optimize(
            lambda weights: -1*r4_f1_wrapper(precomp_text_pre_proc, weights), executor=executor)
        recommendation = np.array(recommendation)
        recommendation.dump('train_weights')

def running5(blah, train='/home/austin/pubs_train.json'):
    train = data.load_data(train)
    labels = data.load_labels('/home/austin/assignment_train.json')

    print('Start')

    authors = set(train['author_searched'])
    authors = np.array(list(authors))
    np.random.shuffle(authors)

    precomp_text_pre_proc = []
    for author in authors[0:3]:
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        text_pre_proc = CountVectorizer(analyzer='char_wb',
                                        ngram_range=(4,4)).fit_transform(train_batch)
        text_pre_proc = TfidfTransformer().fit_transform(text_pre_proc)
        text_pre_proc = TruncatedSVD(n_components=120).fit_transform(text_pre_proc)

        labs = None
        for i, val in enumerate(labels['id_sets']):
            if labels['author_searched'][i] == author:
                labs = val
                break

        ids_batch = [train['id'][i] for i in indexes_used]
        precomp_text_pre_proc.append({
            'author': author,
            'docs': text_pre_proc,
            'ids': ids_batch,
            'labs': labs
        })

    # optimizer = optimizerlib.NoisyBandit(dimension=total_weights, budget=500, num_workers=5)
    optimizer = optimizerlib.OnePlusOne(dimension=total_weights, budget=50, num_workers=5)
    # optimizer = optimizerlib.PSO(dimension=total_weights, budget=100, num_workers=5)
    with futures.ThreadPoolExecutor(max_workers=optimizer.num_workers) as executor:
        recommendation = optimizer.optimize(
            lambda weights: -1*r5_wrapper(precomp_text_pre_proc, weights), executor=executor)
        recommendation = np.array(recommendation)
        recommendation.dump('train_weights')

    if blah:
        train = data.load_data('/home/austin/pubs_validate.json')
    else:
        train = data.load_data('/home/austin/pubs_train.json')
        labels = data.load_labels('/home/austin/assignment_train.json')

    for author in authors:
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        text_pre_proc = CountVectorizer(analyzer='char_wb',
                                        ngram_range=(4,4)).fit_transform(train_batch)
        text_pre_proc = TfidfTransformer().fit_transform(text_pre_proc)
        text_pre_proc = TruncatedSVD(n_components=120).fit_transform(text_pre_proc)

        if not blah:
            for i, val in enumerate(labels['id_sets']):
                if labels['author_searched'][i] == author:
                    labs = val
                    break

        ids_batch = [train['id'][i] for i in indexes_used]

        def preds_wrapper(red_train, **kwargs):
            predicted = TF_NN_NEVER_GRAD(red_train, recommendation)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])
            print('Done...')
            return preds

        def f1_wrapper(red_train, weights, **kwargs):
            # predicted = predictor.fit_predict(docs)
            # print(red_train)
            predicted = TF_NN_NEVER_GRAD(red_train, weights)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])

            f1_val = f1_mod(labs, preds)
            print('F1 metric for {} yields {}'.format(author, f1_val))
            return f1_val

        if not blah:
            f1_wrapper(text_pre_proc, recommendation)

        if blah:
            preds = preds_wrapper(text_pre_proc)
            out[author] = preds
    json.dump(out, open('./predicted_clusters.json', 'w+'))



def running2s(blah):
    if blah:
        # train = data.load_data('/home/austin/pubs_validate.json')
        train = data.load_data('/home/austin/pubs_test.json')
    else:
        train = data.load_data('/home/austin/pubs_train.json')
        labels = data.load_labels('/home/austin/assignment_train.json')

    text_pre = Pipeline([
        ('vect', CountVectorizer(analyzer='char_wb', ngram_range=(4,4))),
        ('tfid', TfidfTransformer()),
        ('embed', TruncatedSVD(n_components=120)),
    ])

    print('Start')

    authors = set(train['author_searched'])
    authors = sorted(authors,
            key=lambda author: len([1 for a in train['author_searched'] if a == author]))

    out = {}
    for author in authors:
        train_batch = []
        indexes_used = []
        for i, val in enumerate(train['docs']):
            if train['author_searched'][i] == author:
                train_batch.append(val)
                indexes_used.append(i)

        labs = None
        if not blah:
            for i, val in enumerate(labels['id_sets']):
                if labels['author_searched'][i] == author:
                    labs = val
                    break

        ids_batch = [train['id'][i] for i in indexes_used]

        def preds_wrapper(pre_proc, docs, **kwargs):
            vecs = pre_proc.fit_transform(docs)
            # predicted = GaussianMixture(n_components=1000).fit_predict(vecs)
            # predicted = BayesianGaussianMixture(n_components=1000).fit_predict(vecs)
            # predicted = GaussianMixture(n_components=(3*len(docs))//4).fit_predict(vecs)
            predicted = AffinityPropagation().fit_predict(vecs)
            if 2*len(set(predicted)) < len(docs):
                num_clsts = 2*len(set(predicted))
            else:
                num_clsts = len(set(predicted))
            predicted = AgglomerativeClustering(n_clusters=num_clsts).fit_predict(vecs)
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])
            print('Done...')
            return preds

        def f1_wrapper(pre_proc, docs, **kwargs):
            vecs = pre_proc.fit_transform(docs)
            # num_clsts = len(set(AffinityPropagation(damping=.9).fit_predict(vecs)))
            # print('ratio: {}'.format(len(labs)/len(docs)))
            # return
            # print('Diff in clusters {}'.format((num_clsts - len(labs))/len(docs)))
            # return
            # predicted = KMeans(n_clusters=num_clsts+10).fit_predict(vecs)
            # try add 162 to clusters
            # predicted = GaussianMixture(n_components=1000).fit_predict(vecs)
            # print(len(labs))
            # return
            predicted = AffinityPropagation().fit_predict(vecs)
            # print(len(set(predicted))/len(labs))
            if 2*len(set(predicted)) < len(docs):
                num_clsts = 2*len(set(predicted))
            else:
                num_clsts = len(set(predicted))
            predicted = AgglomerativeClustering(n_clusters=num_clsts).fit_predict(vecs)
            # Diff in clusters 0.0016701461377870565
            # Diff in clusters -0.01295788686768004
            # Diff in clusters -0.06614491290078263
            # Diff in clusters -0.0817666368248284
            # Diff in clusters -0.0519559902200489
            # Diff in clusters -0.012004001333777926
            # Diff in clusters -0.016195727084769126
            # Diff in clusters -0.07597173144876325
            # Diff in clusters -0.09152035861038475
            # Diff in clusters -0.11841584158415841
            # Diff in clusters -0.11743630573248408
            # Diff in clusters -0.12875359934183464
            # Diff in clusters -0.007534533277521975
            # Diff in clusters -0.06323283082077052
            # Diff in clusters -0.03909643788010426
            preds = zip(ids_batch, predicted)
            temp = [i for i in preds]
            preds = []
            for t in {i[1] for i in temp}:
                preds.append([i[0] for i in temp if i[1] == t])

            f1_val = f1_mod(labs, preds)
            print('F1 metric for {} yields {}'.format(author, f1_val))
            return f1_val

        # params = {
        #    'clst__damping': .9,
        # }
        # text_pre.set_params(**params)

        if not blah:
            f1_wrapper(text_pre, train_batch)

        if blah:
            preds = preds_wrapper(text_pre, train_batch)
            out[author] = preds
    json.dump(out, open('./predicted_clusters.json', 'w+'))

@click.command()
@click.argument('train', type=click.Choice(['train', 'predict']))
def main(train):
    # if train == 'train':
    #     # running4()
    #     running5(False)
    # else:
    #     blah = True
    #     running3(blah)

    # best results
    if train == 'train':
        running2(False)
    else:
        running2(True)


main()
