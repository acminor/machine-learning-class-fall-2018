import tensorflow as tf
from tensorflow.python import debug as tfd
import zhusuan as zs
import numpy as np
import random
from PIL import Image

def shuffle(*args):
    def swap(array, i, j):
        temp = array[i]
        array[i] = array[j]
        array[j] = temp

    for i in range(0,len(args[0])):
        random_index = random.randint(0, len(args[0])-1)
        for j in range(0,len(args)):
            swap(args[j], i, random_index)


def np_one_hot(arr, rank):
    out = []
    for i in arr:
        temp = np.zeros(rank, dtype=np.int32)
        temp[i] = 1
        out.append(temp)
    return np.array(out)


class VAE:
    def __init__(self):
        mnist_train, mnist_test = tf.keras.datasets.mnist.load_data()
        self.image = None

        self.mnist_x_train = mnist_train[0]
        self.mnist_y_train = np_one_hot(mnist_train[1], 10)
        self.mnist_ys = np_one_hot(range(0, 10), 10)

        self.xdim = 784
        self.zdim = 50
        self.ydim = 10

        self.mnist_x_train = np.array([np.reshape(i, [self.xdim]) for i in self.mnist_x_train])
        self.mnist_x_train = self.mnist_x_train/255.0 # problems with NAN otherwise
        self.model()

    def model(self):
        batch_x = tf.placeholder(dtype=tf.float32, shape=(None, self.xdim), name='batch_x')
        batch_y = tf.placeholder(dtype=tf.int32, shape=(None, self.ydim), name='batch_y')
        # batch_y = tf.placeholder(dtype=tf.int32, shape=(1, None, self.ydim), name='batch_y')
        self.lowerBound(batch_x, batch_y)

    # nsamples used to match one hot dimensions in tensorflow
    @zs.reuse('model')
    def theta(self, observed, n_samps, batch_y):
        with zs.BayesianNet(observed=observed) as model:
            _mean = tf.zeros([self.BATCH_SIZE, self.zdim], dtype=tf.float32)
            z = zs.Normal('z', mean=_mean, std=1.0, group_ndims=1)
            def nn(y, z):
                y = tf.to_float(y)
                ny = tf.layers.dense(y, 500, activation=tf.nn.relu)
                nz = tf.layers.dense(z, 500, activation=tf.nn.relu)
                nn = tf.concat([ny, nz], 1)
                # nn = tf.layers.dropout(nn)
                nn = tf.layers.dense(nn, 500, activation=tf.nn.relu)
                # cannot be relu
                nn = tf.layers.dense(nn, self.xdim)
                return nn
            raw_x = zs.Implicit('raw_x', nn(batch_y, z))
            x = zs.Bernoulli('x', logits=raw_x, group_ndims=1, dtype=tf.float32)
        return model

    @zs.reuse('phi')
    def phi(self, batch_x):
        with zs.BayesianNet() as phi:
            def nn(x):
                nx = tf.layers.dense(x, 500, activation=tf.nn.relu)
                nx = tf.layers.dense(nx, 500, activation=tf.nn.relu)
                # nx = tf.layers.dropout(nx)
                # cannot be relu
                nmean = tf.layers.dense(nx, self.zdim)
                nvar = tf.layers.dense(nx, self.zdim)
                return nmean, nvar
            nmean, nvar = nn(tf.to_float(batch_x))
            z = zs.Normal('z', mean=nmean, logstd=nvar, group_ndims=1)
        return phi

    LEARNING_RATE=0.001
    def lowerBound(self, batch_x, batch_y):
        def log_joint(observed):
            model = self.theta(self, observed, 1, batch_y)
            log_pz, log_px_z = model.local_log_prob(['z', 'x'])
            return log_pz + log_px_z
        samps, logs = self.phi(self, batch_x).query('z', outputs=True, local_log_prob=True)
        self.lowerBound = zs.variational.elbo(log_joint=log_joint,
                                              observed={'x': batch_x},
                                              latent={'z': [samps, logs]})
        self.sgvb = self.lowerBound.sgvb()
        self.optimizer = tf.train.AdamOptimizer(self.LEARNING_RATE)
        self.infer_op = self.optimizer.minimize(self.sgvb)

        self.image = tf.sigmoid(self.theta(self, {}, 1, batch_y).outputs('raw_x'))

    EPOCHS = 1500
    BATCH_SIZE = 200
    def main(self):
        with tf.Session() as sess:
            # sess = tfd.LocalCLIDebugWrapperSession(sess)
            sess.run(tf.global_variables_initializer())

            for epoch in range(0, self.EPOCHS):
                shuffle(self.mnist_x_train, self.mnist_y_train)
                lower_bounds = []
                for j in range(0, len(self.mnist_x_train)-1, self.BATCH_SIZE):
                    batch_x = self.mnist_x_train[j:j+self.BATCH_SIZE]
                    batch_y = self.mnist_y_train[j:j+self.BATCH_SIZE]
                    # batch_y = np.reshape(batch_y, [1, self.BATCH_SIZE, self.ydim])
                    _, lower_bound = sess.run([self.infer_op, self.lowerBound],
                             feed_dict={
                                 'batch_x:0': batch_x,
                                 'batch_y:0': batch_y
                             })
                    lower_bounds.append(lower_bound)
                if epoch % 10 == 0:
                    for i, one_hot in enumerate(self.mnist_ys):
                        image = sess.run(self.image, feed_dict={
                            'batch_y:0': np.reshape(np.tile(one_hot, 200), [200, self.ydim])})
                        image = np.reshape(image, [200, 28, 28])
                        # scale = 0.0
                        scale = 255.0
                        image = scale * np.concatenate(image, axis=0)
                        print(image.shape)
                        img = Image.fromarray(image).convert('L')
                        img.save('images/epoch-{}-number-{}.jpeg'.format(epoch, i))
                print(np.mean(lower_bounds))

if __name__ == '__main__':
    v = VAE()
    v.main()
