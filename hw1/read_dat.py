import numpy as np

def read_data_file(filename):
    with open(filename) as file:
        dat = file.read()
    size = len([l for l in dat.split('\n') if l])

    i = 0
    class_dat = np.zeros(size,dtype=np.int8)
    feat_dat = np.full((size, 123), 0.0)
    for line in [l for l in dat.split('\n') if l]:
        params = line.split(' ')
        # normalize to range 0 to 1
        # classification = int(params[0])
        classification = (int(params[0]) + 1) / 2
        class_dat[i] = classification
        features = params[1:]
        for feature in [f for f in features if f]:
            temp = feature.split(':')
            # since feat 1 indexed
            feat = int(temp[0]) - 1
            val = int(temp[1])
            feat_dat[i][feat] = val
        i += 1

    return (class_dat, feat_dat)
