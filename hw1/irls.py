import numpy as np
import numpy.linalg as npl
import math
import random

# X is matrix of training data (features)
# y is column vector of training data (categories)
def w_mat(X):
    w = np.zeros(len(X[:,0]))
    for i in range(0,len(w)):
        w[i] = 1.0/len(w)
    return w

def R_mat(mu):
    R = np.zeros((len(mu), len(mu)))
    for i in range(0,len(np.diagonal(R))):
        R[i,i] = mu[i]*(1-mu[i])
    return R

def psi_mat(x, w):
    # standard logistic sigmoid
    # https://en.wikipedia.org/wiki/Logistic_function
    L = 1.0
    k = 1.0
    x_0 = 0.0
    try:
        output = L/(1+math.exp(-k*(np.vdot(x,w) - x_0)))
    except:
        # 1 / (1 + 1/inf)
        if np.vdot(x, w) > 0:
            output = 1
        # 1 / (1 + inf)
        else:
            output = 0
    return output

def mu_mat(X, w):
    mu = np.zeros(len(X[0,:]))
    for i in range(0, len(mu)):
        mu[i] = psi_mat(X[:,i], w)
    return mu

def z_mat(X, w, R, mu, y, batch_size, full_size):
    output = np.transpose(X)@w - (batch_size/full_size)*npl.inv(R)@(mu-y)
    return output

def shuffle(X, y):
    length = len(y)
    for i in range(0,length):
        rnd_index = random.randint(0, length-1)
        temp = X[:,i]
        X[:, i] = X[:, rnd_index]
        X[:, rnd_index] = temp
        temp = y[i]
        y[i] = y[rnd_index]
        y[rnd_index] = temp
    return X, y

def _w_next_step(X,y,w_init, l2_factor, batch_size, full_size):
    mu = mu_mat(X, w_init)

    R = R_mat(mu)
    R = R + .00001 * np.identity(len(mu))

    # H = hessian_mat(X, w_init)
    # guarantee invertability
    # H = H + .00001 * np.identity(len(H[:,0])) + np.full(H.shape, l2_factor)

    z = z_mat(X, w_init, R, mu, y, batch_size, full_size)
    t = X@R@np.transpose(X) + .00001 * np.identity(len(X[:, 0]))
    t = t + l2_factor * np.identity(len(t[0,:]))

    w = npl.inv(t)@X@R@z
    # w = w_init - npl.inv(-t)@X@(y-mu)
    return w

def w_next_step(X,y,w_init,l2_factor,batch_size):
    shift=0
    for j in range(0, int(math.ceil(len(X[0, :]) / batch_size))):
        start = j*batch_size + shift
        end = (j+1)*batch_size + shift
        w_init = _w_next_step(X[:,start:end],y[start:end], w_init, l2_factor, batch_size, len(y))
    return w_init

def predict(val):
    return 0 if val < 0.5 else 1

def test(X, w, y):
    y_p = [predict(psi_mat(X[:,i], w)) for i in range(0,len(y))]
    a = [1 if y[i] == y_p[i] else 0 for i in range(0,len(y))]
    return sum(a)/len(a)

def irls(X, y, threshold, l2_factor=0, batch_size=100):
    def opt(X,w,y):
        mu = mu_mat(X, w)
        t = (1/len(X[0,:]))*(X@(mu - y))
        return t@t
    i = 1
    w_new = w_mat(X)
    while opt(X, w_new, y) > threshold:
        print('-'*60)
        print("Beginning Iteration: {}".format(i))
        i += 1
        X, y = shuffle(X,y)
        w_new = w_next_step(X, y, w_new, l2_factor, batch_size)
        print("Weighted Gradient: {}".format(opt(X, w_new, y)))
        print("Prediction Accuracy: {}".format(test(X, w_new, y)))
    return w_new

if __name__ == '__main__':
    from read_dat import read_data_file
    import argparse

    parser = argparse.ArgumentParser(description='IRLS algorithm for datasets "a9a" and "a9a.t"')
    parser.add_argument('--batch-size', type=int, default=100)
    parser.add_argument('--lambda-reg', type=float, default=0)
    parser.add_argument('--threshold', type=float, default=0.001)
    args = parser.parse_args()

    y,X = read_data_file('a9a')
    y_p,X_p = read_data_file('a9a.t')
    X = np.transpose(X)
    X_p = np.transpose(X_p)
    w = irls(X, y, args.threshold, l2_factor=args.lambda_reg, batch_size=args.batch_size)
    print('='*60)
    print("Prediction Accuracy (Training): {}".format(test(X, w, y) * 100.0))
    print("Prediction Accuracy (Test): {}".format(test(X_p, w, y_p) * 100.0))
